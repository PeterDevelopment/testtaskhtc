﻿using BlLibrary.Enums;
using DbRepository.Contexts;
using DbRepository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository
{
    /// <summary>
    /// Сервис для работы с Заметками из БД
    /// </summary>
    public class NoteDbService : BasicDbService
    {
        
        /// <summary>
        /// Асинхронный метод возвращает заметку по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Note> GetNoteAsync(int id) => await _db.Notes.FindAsync(id);
        /// <summary>
        /// Синхронный метод возвращает заметку по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Note GetNote(int id) => _db.Notes.Find(id);

        /// <summary>
        /// Добавляет запись заметки в БД асинхронно
        /// </summary>
        /// <param name="note">Объект заметки</param>
        /// <returns>true-успешно/false-неуспешно</returns>
        public async Task<bool> CreateNoteAsync(Note note)
        {
            if (note != null)
            {
                _db.Notes.Add(note);
                try
                {
                    await _db.SaveChangesAsync();
                    return true;
                }
                catch (Exception)
                {
                    //log error
                    return false;
                }
            }
            else return false;
            
        }
        /// <summary>
        /// Сохраняет измененныую заметку
        /// </summary>
        /// <param name="note">Измененная заметка</param>
        /// <returns>true - успешное сохранение/ false - неудачная попытка сохранения</returns>
        public async Task<bool> SaveNoteAsync(Note note)
        {
            if (note != null)
            {
                _db.Entry(note).State = EntityState.Modified;
                try
                {
                    await _db.SaveChangesAsync();
                    return true;
                }
                catch (Exception)
                {
                    //записать в лог
                }
            }
            return false;
        }

        /// <summary>
        /// Асинхронный метод возвращает коллецию заметок в зависимости от режима отображения
        /// </summary>
        /// <param name="displayMode">Режим отображения</param>
        /// <param name="date">Дата относительно, которой необходимо отражать данные</param>
        /// <returns></returns>
        public async Task<IEnumerable<Note>> GetNotesAsync(NoteDisplayMode displayMode, DateTime date)
        {
            IQueryable<Note> notes = _db.Notes;
            switch (displayMode)
            {
                case NoteDisplayMode.List:
                    return await notes.OrderBy(e=>e.StartingDate).ToListAsync();
                case NoteDisplayMode.Mounth:
                    return await notes.Where(e=>e.StartingDate.Month==date.Month).OrderBy(e => e.StartingDate).ToListAsync();
                case NoteDisplayMode.Week:
                    DateTime startDate = date.Subtract(new TimeSpan((int)date.DayOfWeek-1, 0, 0, 0));
                    DateTime endDate = date.AddDays(7-(int)date.DayOfWeek);
                    return await notes.Where(e => DbFunctions.TruncateTime(e.StartingDate) >= DbFunctions.TruncateTime(startDate) && DbFunctions.TruncateTime(e.StartingDate) <= DbFunctions.TruncateTime(endDate)).OrderBy(e=>e.StartingDate).ToListAsync();
                case NoteDisplayMode.Day:
                    return await _db.Notes.Where(e => DbFunctions.TruncateTime(e.StartingDate) == DbFunctions.TruncateTime(date)).OrderBy(e => e.StartingDate).ToListAsync();
                default:
                    return (await GetNotesAsync()).ToList();
            }
        }

        /// <summary>
        /// Асинхронный метод возвращает результаты поиска
        /// </summary>
        /// <param name="searchType">Тип поиска</param>
        /// <param name="searchText">строка поиска</param>
        /// <returns></returns>
        public async Task<IEnumerable<Note>> SearchNotesAsync(NoteSearchType searchType, string searchText)
        {
            if (!string.IsNullOrEmpty(searchText))
            {
                switch (searchType)
                {
                    case NoteSearchType.Date:
                        if (DateTime.TryParse(searchText, out DateTime date))
                        {
                            return await _db.Notes.Where(e => DbFunctions.TruncateTime(e.StartingDate) == DbFunctions.TruncateTime(date)).ToListAsync();
                        }
                        else return new List<Note>();
                    case NoteSearchType.Other:
                        return await _db.Notes.Where(e => e.MeetingPlace.ToUpper().Contains(searchText.ToUpper()) || e.SubjectNote.ToUpper().Contains(searchText.ToUpper())).ToListAsync();
                default:
                        return new List<Note>();
                }
            } else return new List<Note>();
            
        }
        /// <summary>
        /// Метод удаляет заметку по Id заметки
        /// </summary>
        /// <param name="id">Id заметки</param>
        /// <returns>возвращает true при успешном удалении иначе вернет false</returns>
        public bool DeleteNote(int id)
        {
            Note note = _db.Notes.Find(id);
            if (note != null)
            {
                _db.Notes.Remove(note);
                try
                {
                    _db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    //записать в лог ошибку
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// Синхронный метод, меняющий статус заметки
        /// </summary>
        /// <param name="isDone">статус заявки на который необходимо сменить</param>
        /// <param name="id">Id заметки, у которой необходимо сменить статус</param>
        /// <returns>true - успешное выполнение, false - неуспешное выполнение</returns>
        public bool ChangeNoteStatus(bool isDone, int id)
        {
            Note note = _db.Notes.Find(id);
            if (note != null)
            {
                note.IsDone = isDone;
                _db.Entry(note).State = EntityState.Modified;
                try
                {
                    _db.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    //записать в лог ошибку
                    return false;
                }
            }
            return false;
        }
        /// <summary>
        /// Метод возвращает записи заданного типа
        /// </summary>
        /// <param name="noteType">Тип записи</param>
        /// <returns></returns>
        public async Task<IEnumerable<Note>> GetNotesByTypeAsync(int noteType) => await _db.Notes.Where(e => e.NoteType == noteType).ToListAsync();
        

        /// <summary>
        /// Асинхронный метод возвращает коллекцию всех заметок
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Note>> GetNotesAsync() => await _db.Notes.OrderByDescending(e=>e.StartingDate).ToListAsync();

    }
}
