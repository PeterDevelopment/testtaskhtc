﻿using DbRepository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Contexts
{
    internal class DataBaseContext : DbContext
    {
        static DataBaseContext()
        {
            Database.SetInitializer(new DbInitializer());
        }

        internal DataBaseContext()
        {
            Database.Connection.ConnectionString = Database.Connection.ConnectionString = "data source=(LocalDb)\\MSSQLLocalDB;initial catalog=TestHTCDb;integrated security=True;MultipleActiveResultSets=True;";
        }
        public virtual DbSet<Note> Notes { get; set; }
        public virtual DbSet<Person> People { get; set; }

    }
}
