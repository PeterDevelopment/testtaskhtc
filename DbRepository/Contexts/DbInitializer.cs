﻿using DbRepository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Contexts
{
    internal class DbInitializer : CreateDatabaseIfNotExists<DataBaseContext>
    {
        protected override void Seed(DataBaseContext context)
        {
            List<Note> notes = new List<Note>
            {
                new Note{SubjectNote = "Встреча", NoteType=0, StartingDate=DateTime.Parse("01.04.2021 14:58:32"),MeetingPlace="место",EndingDate = DateTime.Parse("01.04.2021 15:58:32"), IsDone=true},
                new Note{SubjectNote = "Памятка 1", NoteType=1, StartingDate=DateTime.Parse("02.04.2021 14:58:32")},
                new Note{SubjectNote = "Памятка 2", NoteType=1, StartingDate=DateTime.Parse("02.04.2021 11:58:32"), IsDone=true},
                new Note{SubjectNote = "Встреча", NoteType=0, StartingDate=DateTime.Parse("02.04.2021 16:58:32"),MeetingPlace="место",EndingDate = DateTime.Parse("02.04.2021 17:58:32"), IsDone=true},
                new Note{SubjectNote = "Дело 1", NoteType=2, StartingDate=DateTime.Parse("03.04.2021 08:00:00"),EndingDate = DateTime.Parse("04.04.2021 10:00:00"), IsDone=true},
                new Note{SubjectNote = "Дело 2", NoteType=2, StartingDate=DateTime.Parse("03.04.2021 09:00:32"),EndingDate = DateTime.Parse("03.04.2021 10:00:00"), IsDone=true},
                new Note{SubjectNote = "Встреча", NoteType=0, StartingDate=DateTime.Parse("03.04.2021 12:00:00"),MeetingPlace="место",EndingDate = DateTime.Parse("03.04.2021 13:00:00")},
                new Note{SubjectNote = "Памятка 3", NoteType=1, StartingDate=DateTime.Parse("03.04.2021 14:00:00"), IsDone=true},
                new Note{SubjectNote = "Памятка 4", NoteType=1, StartingDate=DateTime.Parse("03.04.2021 15:00:00"), IsDone=true},
                new Note{SubjectNote = "Дело 3", NoteType=2, StartingDate=DateTime.Parse("04.04.2021 08:00:32"),EndingDate = DateTime.Parse("04.04.2021 09:00:00"), IsDone=true},
                new Note{SubjectNote = "Встреча", NoteType=0, StartingDate=DateTime.Parse("04.04.2021 11:00:00"),MeetingPlace="место",EndingDate = DateTime.Parse("04.04.2021 12:00:00"), IsDone=true},
                new Note{SubjectNote = "Памятка 5", NoteType=1, StartingDate=DateTime.Parse("01.04.2021 14:58:32"), IsDone=true}
            };
            notes.ForEach(e =>
            {
                context.Notes.Add(e);
            });
            
            List<Person> persons = new List<Person>
            {
                new Person{ LastName="Иванов", FirstName="Иван", MiddleName="Сидорович",Company="ООО \"Рога и Копыта\"", BirthDate=DateTime.Parse("12.05.1985"), JobPosition="Директор", Contacts=new List<PersonContact>{ new PersonContact { ContactType = 1, Contact = "ivanov@randk.ru" }, new PersonContact { ContactType = 0, Contact = "79276546565" }, new PersonContact{ ContactType=2, Contact="ivanov"} } },
                new Person{ LastName="Сидоров", FirstName="Сидор", MiddleName="Сидорович",Company="КПК Финансы", BirthDate=DateTime.Parse("12.03.1975"), JobPosition="Директор", Contacts=new List<PersonContact>{ new PersonContact { ContactType = 1, Contact = "sidorov@finance.ru" },new PersonContact{ ContactType = 0, Contact = "79024560000" }, new PersonContact { ContactType = 0, Contact = "79188650000" }, new PersonContact{ ContactType = 1, Contact ="sidorov@yandex.ru"} } },
                new Person{ LastName="Петров", FirstName="Петр", MiddleName="Петрович",Company="ООО \"Магазин\"", BirthDate=DateTime.Parse("02.05.1990"), JobPosition="Менеджер", Contacts=new List<PersonContact>{ new PersonContact { ContactType = 1, Contact = "petrov@ya.ru" },new PersonContact{ ContactType = 0, Contact = "79456235577" } } },
                new Person{ LastName="Иванова", FirstName="Елена", MiddleName="Сергеевна",Company="ООО \"Магазин\"", BirthDate=DateTime.Parse("12.07.1995"), JobPosition="Бухгалтер", Contacts=new List<PersonContact>{ new PersonContact { ContactType = 1, Contact = "ivanova@bk.ru" }, new PersonContact { ContactType = 0, Contact = "79165550000" } } },
                new Person{ LastName="Пупкин", FirstName="Харитон", MiddleName="Елисеевич",Company="КПК Финансы", BirthDate=DateTime.Parse("10.09.1990"), JobPosition="Менеджер", Contacts=new List<PersonContact>{ new PersonContact { ContactType = 1, Contact = "pupkin@gmail.com" }, new PersonContact{ ContactType = 0, Contact = "79126355632" } } },
                new Person{ LastName="Ерофеева", FirstName="Ерофея", MiddleName="Ерофеевна",Company="КПК Финансы", BirthDate=DateTime.Parse("12.01.2000"), JobPosition="Бухгалтер", Contacts=new List<PersonContact>{ new PersonContact { ContactType = 1, Contact = "erofeeva@bk.ru" }, new PersonContact{ ContactType = 0, Contact = "79005654212" } } },
                new Person{ LastName="Елисарова", FirstName="Елисавета", MiddleName="Елисаровна",Company="ООО \"Магазин\"", BirthDate=DateTime.Parse("12.05.1980"), JobPosition="Директор", Contacts=new List<PersonContact>{ new PersonContact { ContactType = 1, Contact = "elisarova@gmail.com" }, new PersonContact { ContactType = 0, Contact = "79021002222" }, new PersonContact { ContactType = 1, Contact = "elisarova@bk.ru" }, new PersonContact{ ContactType = 0, Contact = "79521231515" } } },
                new Person{ LastName="Сидоров", FirstName="Иван", MiddleName="Харитонович",Company="ООО \"Рога и Копыта\"", BirthDate=DateTime.Parse("10.05.1985"), JobPosition="Менеджер", Contacts=new List<PersonContact>{ new PersonContact { ContactType = 1, Contact = "sidorov@bk.ru" }, new PersonContact { ContactType = 0, Contact = "79365214444" } } }
            };
            persons.ForEach(e =>
            {
                context.People.Add(e);
            });
            context.SaveChanges();
        }
    }
}
