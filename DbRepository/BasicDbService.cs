﻿using DbRepository.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository
{
    /// <summary>
    /// Базовый класс для сервиса работы с БД
    /// </summary>
    public class BasicDbService : IDisposable
    {
        private protected readonly DataBaseContext _db;
        public BasicDbService()
        {
            _db = new DataBaseContext();
        }

        public virtual void Dispose()
        {
            _db.Dispose();
        }
    }
}
