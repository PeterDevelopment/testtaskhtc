﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Models
{
    public class Note
    {
        /// <summary>
        /// Id 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Тема заметки
        /// </summary>
        public string SubjectNote { get; set; }
        /// <summary>
        /// Дата и время начала
        /// </summary>
        public DateTime StartingDate { get; set; }
        /// <summary>
        /// Дата и время окончания
        /// </summary>
        public DateTime? EndingDate { get; set; }
        /// <summary>
        /// Тип заметки
        /// </summary>
        public byte NoteType { get; set; }
        /// <summary>
        /// Место встречи
        /// </summary>
        public string MeetingPlace { get; set; }
        /// <summary>
        /// Флаг выполнения заметки
        /// </summary>
        public bool IsDone { get; set; }
    }
}
