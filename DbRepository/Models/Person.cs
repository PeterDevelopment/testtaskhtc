﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Models
{
    public class Person
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Фамилиля
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime BirthDate { get; set; }
        /// <summary>
        /// Организация
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// Должность
        /// </summary>
        public string JobPosition { get; set; }
        /// <summary>
        /// Контакты
        /// </summary>
        public virtual ICollection<PersonContact> Contacts { get; set; }
    }
}
