﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Models
{
    public class PersonContact
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Тип контакта
        /// </summary>
        public byte ContactType { get; set; }
        /// <summary>
        /// Содержимое контакта
        /// </summary>
        public string Contact { get; set; }
    }
}
