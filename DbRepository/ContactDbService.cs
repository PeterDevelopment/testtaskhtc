﻿using DbRepository.Contexts;
using DbRepository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository
{
    /// <summary>
    /// Сервис для работы с Контактами из БД
    /// </summary>
    public class ContactDbService : BasicDbService
    {
        /// <summary>
        /// Асинхронно возвращает все контакты
        /// </summary>
        /// <returns>Коллекция контактов</returns>
        public async Task<IEnumerable<Person>> GetContacts() => await _db.People.ToListAsync();

        /// <summary>
        /// Асинхронно возвращает Person по Id 
        /// </summary>
        /// <param name="personId">Id Person</param>
        /// <returns></returns>
        public async Task<Person> GetPersonAsync(int personId) => await _db.People.FindAsync(personId);

        /// <summary>
        /// Добавляет запись контакта в БД асинхронно
        /// </summary>
        /// <param name="person">объект Person</param>
        /// <returns></returns>
        public async Task<bool> AddContactAsync(Person person)
        {
            if (person != null)
            {
                _db.People.Add(person);
                try
                {
                    await _db.SaveChangesAsync();
                    return true;
                }
                catch (Exception)
                {
                    //записать в лог
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// Сохраняет запись контакта в БД асинхронно
        /// </summary>
        /// <param name="person">объект Person</param>
        /// <returns></returns>
        public async Task<bool> SaveContactAsync(Person person)
        {
            if (person != null)
            {
                if (person.Id != 0)
                {
                    Person personToUpdate = await _db.People.FindAsync(person.Id);
                    if (personToUpdate != null)
                    {
                        personToUpdate.Contacts.ToList();
                        personToUpdate.BirthDate = person.BirthDate;
                        personToUpdate.Company = person.Company;
                        personToUpdate.LastName = person.LastName;
                        personToUpdate.FirstName = person.FirstName;
                        personToUpdate.MiddleName = person.MiddleName;
                        personToUpdate.JobPosition = person.JobPosition;
                        personToUpdate.Contacts
                            .Where(e => !person.Contacts.ToList().Exists(k=>k.Contact==e.Contact && k.ContactType==e.ContactType))
                            .ToList()
                            .ForEach(n=> {
                                _db.Entry(n).State = EntityState.Deleted;
                            });
                        person.Contacts
                            .Where(e => !personToUpdate.Contacts.ToList().Exists(k => k.Contact == e.Contact && k.ContactType == e.ContactType))
                            .ToList()
                            .ForEach(n=> {
                                personToUpdate.Contacts.Add(n);
                                _db.Entry(n).State = EntityState.Added;
                            });
                        try
                        {
                            await _db.SaveChangesAsync();
                            return true;
                        }
                        catch (Exception)
                        {
                            //записать в лог
                            return false;
                        }
                    }
                    
                }
            }
            return false;
        }

        /// <summary>
        /// Возвращает результаты поиска по контактам
        /// </summary>
        /// <param name="searchText">строка поиска</param>
        /// <returns></returns>
        public async Task<IEnumerable<Person>> SearchPeopleAsync(string searchText)
        {
            if(DateTime.TryParse(searchText, out DateTime date))
            {
                return await _db.People.Where(e => DbFunctions.TruncateTime(e.BirthDate) == DbFunctions.TruncateTime(date)).ToListAsync();
            }
            else
            {
                searchText = searchText.ToUpper();
                var people = await _db.People
                    .Where(e =>
                        e.Company.ToUpper().Contains(searchText) ||
                        e.FirstName.ToUpper().Contains(searchText) ||
                        e.LastName.ToUpper().Contains(searchText) ||
                        e.MiddleName.ToUpper().Contains(searchText) ||
                        e.JobPosition.ToUpper().Contains(searchText) ||
                        e.Contacts.Any(k => k.Contact.ToUpper().Contains(searchText))
                    )
                    .ToListAsync();
                return people;
            }
        }

        /// <summary>
        /// Асинхронно удаляет Person по его Id и возвращает флаг результата выполнения
        /// </summary>
        /// <param name="personId">Id контакта</param>
        /// <returns>true-удаление успешно, false-не удалось удалить</returns>
        public async Task<bool> DeleteContactAsync(int personId)
        {
            Person person = await _db.People.FindAsync(personId);
            if (person != null)
            {
                person.Contacts.ToList();
                _db.People.Remove(person);
                try
                {
                    await _db.SaveChangesAsync();
                    return true;
                }
                catch (Exception)
                {
                    //Записать в лог
                    return false;
                }
            }
            else return false;
        }
        
    }
}
