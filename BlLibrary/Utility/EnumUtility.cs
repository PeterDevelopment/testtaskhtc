﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlLibrary.Utility
{
    /// <summary>
    /// Статический класс для работы с перечислениями
    /// </summary>
    public static class EnumUtility
    {
        /// <summary>
        /// Возвращает содержимое аттрибута Description у enum
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Description(this Enum value) => !(value.GetType().GetField(value.ToString())
                                                                              .GetCustomAttributes(typeof(DescriptionAttribute), false)
                                                                              .SingleOrDefault() is DescriptionAttribute attribute) ?
                                                                                                    value.ToString() : attribute.Description;


        /// <summary>
        /// Метод возвращающий коллекцию элементов в Enum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> GetValues<T>() where T : Enum => Enum.GetValues(typeof(T)).Cast<T>();

        /// <summary>
        /// Возвращает содержимое аттрибута Description у enum по его значению
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescriptionByValue<T>(int value) where T : Enum
        {
            try
            {
                var processingCode = (T)Enum.GetValues(typeof(T)).GetValue(value);
                return processingCode.Description();
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
        }
        /// <summary>
        /// Возвращает Enum по его значению
        /// </summary>
        /// <typeparam name="T">Тип enum</typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T GetEnumByValue<T>(int value) where T : Enum
        {
            try
            {
                return (T)Enum.GetValues(typeof(T)).GetValue(value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
