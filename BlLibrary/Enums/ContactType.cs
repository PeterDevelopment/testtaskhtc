﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlLibrary.Enums
{
    /// <summary>
    /// Перечисление типов контактов
    /// </summary>
    public enum ContactType
    {
        /// <summary>
        /// Телефон
        /// </summary>
        [Description("Телефон")]
        PhoneNumber,
        /// <summary>
        /// Email
        /// </summary>
        [Description("Email")]
        Email,
        /// <summary>
        /// Skype
        /// </summary>
        [Description("Skype")]
        Skype,
        /// <summary>
        /// Иное
        /// </summary>
        [Description("Иное")]
        Other
    }
}
