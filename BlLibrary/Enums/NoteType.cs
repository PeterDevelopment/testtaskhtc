﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlLibrary.Enums
{
    /// <summary>
    /// Тип заметки
    /// </summary>
    public enum NoteType
    {
        /// <summary>
        /// Встреча
        /// </summary>
        [Description("Встреча")]
        Meeting,
        /// <summary>
        /// Памятка
        /// </summary>
        [Description("Памятка")]
        Reminder,
        /// <summary>
        /// Дело
        /// </summary>
        [Description("Дело")]
        Matter
    }
}