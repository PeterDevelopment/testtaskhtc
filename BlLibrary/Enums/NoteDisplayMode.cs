﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlLibrary.Enums
{
    /// <summary>
    /// Режимы отображения записей ежедневника
    /// </summary>
    public enum NoteDisplayMode
    {
        /// <summary>
        /// Список
        /// </summary>
        [Description("Список")]
        List,
        /// <summary>
        /// Месяц
        /// </summary>
        [Description("Месяц")]
        Mounth,
        /// <summary>
        /// Неделя
        /// </summary>
        [Description("Неделя")]
        Week,
        /// <summary>
        /// День
        /// </summary>
        [Description("День")]
        Day
    }
}
