﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlLibrary.Enums
{
    /// <summary>
    /// Перечисление типов поиска
    /// </summary>
    public enum NoteSearchType
    {
        /// <summary>
        /// По дате
        /// </summary>
        Date,
        /// <summary>
        /// По поисковой строке
        /// </summary>
        Other
    }
}
