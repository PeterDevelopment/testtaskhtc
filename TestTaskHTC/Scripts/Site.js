﻿//Создает спиннер в блоке по Id
function CreateSpinner(blockId) {
    var spinner = document.createElement('div');
    spinner.classList.add('spinner-border');
    spinner.setAttribute('role', 'status');
    spinner.innerHTML = '<span class="sr-only">Загрузка...</span>';
    spinner.style.position = 'relative';
    spinner.style.top = '50%';
    spinner.style.left = '50%';
    ClearBlockContent(blockId);
    var blockView = document.getElementById(blockId);
    if (blockView != null) {
        blockView.appendChild(spinner);
    }
}
/*Создает спиннер при создании контакта*/
function CreateSpinnerContact(personListId, detailsInfo) {
    CreateSpinner(personListId);
    CreateSpinner(detailsInfo);
}
//удаляет содержимое блока по Id
function ClearBlockContent(blockId) {
    var blockView = document.getElementById(blockId);
    if (blockView != null) {
        while (blockView.firstChild) {
            blockView.removeChild(blockView.firstChild);
        }
    };
}
/*Обработчик выбора типа заметки*/
function ChangeNoteType() {
    var selectInpt = document.getElementById('inptNoteType');
    if (selectInpt != null) {
        switch (selectInpt.value) {
            case 'Meeting':
                MeetingHandler();
                break;
            case 'Reminder':
                ReminderHandler();
                break;
            case 'Matter':
                MatterHandler();
                break;
            default:
                selectInpt.value = 'Meeting';
                MeetingHandler();
                break;
        }
    }
}
/*отображает поля при выборе заметки "Встреча"*/
function MeetingHandler() {
    var endinDateBlock = document.getElementById('endingDateBlock');
    var meetingBlock = document.getElementById('meetingBlock');
    if (endinDateBlock != null && meetingBlock != null) {
        if (endinDateBlock.classList.contains('d-none')) {
            endinDateBlock.classList.remove('d-none');
        }
        if (meetingBlock.classList.contains('d-none')) {
            meetingBlock.classList.remove('d-none');
        }
    }
}
/*отображает поля при выборе заметки "Заметка"*/
function ReminderHandler() {
    var endinDateBlock = document.getElementById('endingDateBlock');
    var meetingBlock = document.getElementById('meetingBlock');
    if (endinDateBlock != null && meetingBlock != null) {
        if (!endinDateBlock.classList.contains('d-none')) {
            endinDateBlock.classList.add('d-none');
        }
        if (!meetingBlock.classList.contains('d-none')) {
            meetingBlock.classList.add('d-none');
        }
    }
}
/*отображает поля при выборе заметки "Дело"*/
function MatterHandler() {
    var endinDateBlock = document.getElementById('endingDateBlock');
    var meetingBlock = document.getElementById('meetingBlock');
    if (endinDateBlock != null && meetingBlock != null) {
        if (endinDateBlock.classList.contains('d-none')) {
            endinDateBlock.classList.remove('d-none');
        }
        if (!meetingBlock.classList.contains('d-none')) {
            meetingBlock.classList.add('d-none');
        }
    }
}
/*Закрывает модальное окно по Id окна*/
function CloseModal(modalId) {
    $(modalId).modal('hide');
}

function ShowFormat() {
    var selectInput = document.getElementById('selectForContact');
    var label = document.getElementById('labelForSelect');
    if (selectInput != null && label != null) {
        switch (selectInput.value) {
            case '0':
                if (label.classList.contains('d-none')) {
                    label.classList.remove('d-none');
                }
                break;
            default:
                if (!label.classList.contains('d-none')) {
                    label.classList.add('d-none');
                }
        }
    }
}

/*Добавляет контактную информацию на форме добавления контакта*/
function AddContact() {
    var tableBody = document.getElementById('contactsTableBody');

    if (tableBody != null) {
        if (document.getElementById('notFoundContact') != null) {
            ClearBlockContent('contactsTableBody');
        }
        var selectInput = document.getElementById('selectForContact');
        var texboxInput = document.getElementById('inpForContact');
        if (selectInput != null && texboxInput != null) {
            var trTable = document.createElement('tr');
            var trId = 'contDetail-' + counterContacts;
            trTable.setAttribute('id', trId);
            var tdLabel = document.createElement('td');
            var typeContact;
            switch (selectInput.value) {
                case '0':
                    typeContact = 'Телефон';
                    break;
                case '1':
                    typeContact = 'Email';
                    break;
                case '2':
                    typeContact = 'Skype';
                    break;
                default:
                    typeContact = 'Иное';
                    break;
            }
            tdLabel.innerHTML = '<label><b>' + typeContact + '</b></label><input type="number" name="Contacts[' + counterContacts + '].ContactType" value="' + selectInput.value + '" hidden /><input type="text" name="Contacts[' + counterContacts + '].Contact" value="' + texboxInput.value + '" hidden />';
            var tdContent = document.createElement('td');
            tdContent.classList.add('d-flex');
            var functionName = "DeleteBlock('" + trId + "');";
            tdContent.innerHTML = '<div class="p-1"><label>' + texboxInput.value + '</label></div><div class="p-1 flex-grow-1"></div><div class="p-1"><a class="btn btn-sm btn-danger btn-hover p-1" onclick="' + functionName + '">Удалить</a></div>';
            trTable.appendChild(tdLabel);
            trTable.appendChild(tdContent);
            tableBody.appendChild(trTable);
            counterContacts++; //счетчик обнуляется в момент отрисовки частичного представления создания контакта
            texboxInput.value = '';
        }
    }
}

//проводит переиндексацию индексов контактов при удалении и добавлении
function ReIndexContacts() {
    var contactContents = document.getElementsByClassName('contactContent');
    if (contactContents != null)
    {
        counterContacts = 0;
        if (contactContents.length !=0) {
            for (let input of contactContents) {
                var parentTd = input.parentElement;

                var contact = 'Contacts[' + counterContacts + '].Contact';
                var contactType = 'Contacts[' + counterContacts + '].ContactType';
                input.setAttribute('name', contact);
                parentTd.children[2].setAttribute('name', contactType);
                var parentTr = parentTd.parentElement;
                if (parentTr.getAttribute('id').includes('contDetail')) {
                    var trId = 'contDetail-' + counterContacts;
                    parentTr.setAttribute('id', trId);
                    var button = parentTr.children[1].children[2].firstElementChild;
                    var functionName = "DeleteBlock('" + trId + "');";
                    button.addEventListener("click", DeleteBlock(trId));
                };
                counterContacts++;
            };
        };
    }
}
//Очищает содержимое input по его id
function ClearInput(blockId) {
    var block = document.getElementById(blockId);
    if (block != null) {
        block.value = '';
    }
}
//Удаляет блок контакта по Id
function DeleteBlock(blockId) {
    var block = document.getElementById(blockId);
    if (block != null) {
        block.remove();
        ReIndexContacts();
    }
}
//$(function () {
//    $("#inptDate").datepicker($.datepicker.regional["ru"]);
//    $("#inptDate").datepicker("option", "dateFormat", "yy-mm-dd");
//    $("#inptDate").datepicker();
//});

function ChangeTypeInput(blockId) {
    var inpt = document.getElementById(blockId);
    if (inpt != null) {
        inpt.setAttribute('type', 'text');
    }
}

function getInternetExplorerVersion() {
    var rv = -1;
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    else if (navigator.appName == 'Netscape') {
        var ua = navigator.userAgent;
        var re = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}

