﻿using DbRepository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTaskHTC.Models.Validation
{
    /// <summary>
    /// Класс для валидации данных с форм операций с Контактами
    /// </summary>
    public class ContactValidationProvider
    {
        /// <summary>
        /// Валидирует контактную информацию у Person
        /// </summary>
        /// <param name="contacts">Коллекция контактной информации</param>
        /// <returns></returns>
        public IEnumerable<ValidationErrorMessage> ValidateContacts(IEnumerable<PersonContact> contacts)
        {
            List<ValidationErrorMessage> errors = new List<ValidationErrorMessage>();

            if (contacts.ToList().Exists(e => e.ContactType == 0))
            {
                var phones = contacts.AsQueryable().Where(e => e.ContactType == 0).ToList();
                phones.ForEach(e =>
                {
                    string phone = string.Join("", e.Contact.Where(k => char.IsDigit(k)));
                    if (phone.Length != 11) errors.Add(new ValidationErrorMessage { Key="Phone", Message=$"Некорректный номер телефона: {e.Contact}" });
                });
            }
            if (contacts.ToList().Exists(e => e.ContactType == 1))
            {
                var emails = contacts.AsQueryable().Where(e => e.ContactType == 1).ToList();
                emails.ForEach(e =>
                {
                    if (e.Contact.Where(k => k.Equals('@')).Count() == 1)
                    {
                        string[] emailParts;
                        emailParts = e.Contact.Split('@');
                        if (emailParts[1].Where(k => k.Equals('.')).Count() < 1) errors.Add(new ValidationErrorMessage { Key = "Email", Message = $"Некорректный email: {e.Contact}" });
                    }
                    else errors.Add(new ValidationErrorMessage { Key = "Email", Message = $"Некорректный email: {e.Contact}" });
                });
            }

            return errors;
        }
    }
}