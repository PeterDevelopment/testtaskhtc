﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTaskHTC.Models.Validation
{
    /// <summary>
    /// Класс для добавления сообщения в коллекцию ошибок ModelState
    /// </summary>
    public class ValidationErrorMessage
    {
        /// <summary>
        /// Ключ
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// Сообщение об ошибке связанное с заданным ключем
        /// </summary>
        public string Message { get; set; }
    }
}