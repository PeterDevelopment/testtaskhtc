﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTaskHTC.Models
{
    /// <summary>
    /// Класс для отображения сообщения о результате операции
    /// </summary>
    public class ResultOperationMessage
    {
        public bool ForAdding { get; set; }
        /// <summary>
        /// Признак успешной операции
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string Message { get; set; }
    }
}