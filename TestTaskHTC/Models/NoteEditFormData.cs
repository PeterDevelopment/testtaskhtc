﻿using BlLibrary.Enums;
using BlLibrary.Utility;
using DbRepository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTaskHTC.Models
{
    /// <summary>
    /// Объект для вьюхи редактирования заметки
    /// </summary>
    public class NoteEditFormData : NoteFormData
    {
        /// <summary>
        /// Id заметки
        /// </summary>
        public int NoteId { get; set; }
        /// <summary>
        /// Возвращает объект модели для вьюхи по объекту заметки
        /// </summary>
        /// <param name="note">Заметка</param>
        /// <returns></returns>
        public static NoteEditFormData CreateFromNote(Note note) => new NoteEditFormData
        {
            NoteId = note.Id,
            NoteType = EnumUtility.GetEnumByValue<NoteType>(note.NoteType),
            SubjectNote = note.SubjectNote,
            StartingDate = note.StartingDate.ToString(),
            EndingDate = (note.EndingDate!=null)? Convert.ToDateTime(note.EndingDate).ToString() : DateTime.Now.ToString(),
            MeetingPlace = note.MeetingPlace
        };
        
    }
}