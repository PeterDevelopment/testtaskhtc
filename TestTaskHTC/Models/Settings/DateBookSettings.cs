﻿using BlLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTaskHTC.Models.Settings
{
    /// <summary>
    /// Класс хранения состояний для раздела ежедневника для конкретной сессии
    /// </summary>
    public class DateBookSettings
    {
        /// <summary>
        /// Поля инициализируются значениями по умолчанию(Режим отображения: День, Дата: текущая дата)
        /// </summary>
        public DateBookSettings()
        {
            DisplayMode = NoteDisplayMode.Day;
            Date = DateTime.Now;
        }
        /// <summary>
        /// Режим отображения
        /// </summary>
        public NoteDisplayMode DisplayMode { get; set; }
        /// <summary>
        /// Дата относительно, которой строится отображение
        /// </summary>
        public DateTime Date { get; set; }
    }
}