﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTaskHTC.Models.Settings
{
    /// <summary>
    /// Класс хранения состояний приложения для конкретной сессии
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Состояния ежедневника
        /// </summary>
        public DateBookSettings DateBook { get; set; }
    }
}