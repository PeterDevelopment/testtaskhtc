﻿using BlLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTaskHTC.Models
{
    /// <summary>
    /// Объект для вьюхи создания заметки
    /// </summary>
    public class NoteFormData
    {
        /// <summary>
        /// Тема заметки
        /// </summary>
        public string SubjectNote { get; set; }
        /// <summary>
        /// Дата и время начала
        /// </summary>
        public string StartingDate { get; set; }
        /// <summary>
        /// Дата и время окончания
        /// </summary>
        public string EndingDate { get; set; }
        /// <summary>
        /// Тип заметки
        /// </summary>
        public NoteType NoteType { get; set; }
        /// <summary>
        /// Место встречи
        /// </summary>
        public string MeetingPlace { get; set; }
    }
}