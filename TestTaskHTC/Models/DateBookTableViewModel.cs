﻿using BlLibrary.Enums;
using DbRepository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTaskHTC.Models
{
    /// <summary>
    /// ViewModel для таблицы заметок
    /// </summary>
    public class DateBookTableViewModel
    {
        /// <summary>
        /// Режим отображения
        /// </summary>
        public NoteDisplayMode DisplayMode { get; set; }
        /// <summary>
        /// Коллекция заметок для отображения
        /// </summary>
        public IEnumerable<Note> Notes { get; set; }
    }
}