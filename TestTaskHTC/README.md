﻿

### Использованные технологии
- EntityFramework 6.4.4
- ASP.NET MVC 5
- jquery.unobtrusive-ajax.js
- jquery-ui-1.12.1.js
- Microsoft SQL Server
- Net.Framework 4.8
- bootstrap.css
- bootstrap.bundle.min.js
- jquery-3.6.0.js

____

### Инструкция по развертыванию


- При запуске и выполнении проекта необходимо наличие интернета, для загрузки иконок(https://maxst.icons8.com) и корректного отображения кнопок.
- Необходимо наличие установленного MS SQL Express сервера или LocalDb


База данных создается автоматически (при первом обращении и заполняется тестовыми данными) в экземпляре SQL Express или если он не доступен, то попытается использовать <a href="https://docs.microsoft.com/en-us/previous-versions/sql/sql-server-2012/hh510202(v=sql.110)?redirectedfrom=MSDN">LocalDb</a>

#### Не работает в браузере Internet Explorer

Не успел разобраться почему не отрабатывает javascript из файла(даже alert("111") в начале Site.js в IE не отрабатывает). Если содержимое скрипта кинуть на вьюху, то все отрабатывает.
