﻿using BlLibrary.Enums;
using BlLibrary.Utility;
using DbRepository;
using DbRepository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TestTaskHTC.Models;
using TestTaskHTC.Models.Settings;

namespace TestTaskHTC.Controllers
{
    public class DateBookController : Controller
    {
        private readonly NoteDbService _serviceDb;
        public DateBookController()
        {
            _serviceDb = new NoteDbService();
        }
        /// <summary>
        /// Возвращает форму создания записи
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult ShowCreateForm() => PartialView("_CreateNoteForm", null);

        /// <summary>
        /// Возвращает форму редактирования записи
        /// </summary>
        /// <param name="noteId">ID записи для редактирования</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult ShowEditForm(int noteId)
        {
            Note note = _serviceDb.GetNote(noteId);
            NoteEditFormData editNote = null;
            if (note != null) editNote = NoteEditFormData.CreateFromNote(note);
            
            return PartialView("_EditNoteForm", editNote);
        }
        /// <summary>
        /// Возвращает частичное представление с отображением записей в зависимости от режима отображения
        /// </summary>
        /// <param name="displayMode">режим отображения</param>
        /// <param name="date">дата, относительно, которой отображаются записи</param>
        /// <returns></returns>
        public PartialViewResult ShowNotes(NoteDisplayMode displayMode, DateTime date)
        {
            Settings settings = Session["Settings"] as Settings?? new Settings{ DateBook = new DateBookSettings { Date=date} };

            //Проверим создавались ли настройки для ежедневника, если пользователь пришел с другого раздела создадим настройки для ежедневника по умолчанию
            if (settings.DateBook != null) 
            { 
                settings.DateBook.DisplayMode = displayMode;
                settings.DateBook.Date = date;
            }
            else settings.DateBook = new DateBookSettings { DisplayMode = displayMode, Date = date };

            Task<IEnumerable<Note>> task = Task.Run(async () => await _serviceDb.GetNotesAsync(displayMode, date));
            Session["Settings"] = settings;
            return PartialView("_DateBookTable", new DateBookTableViewModel
            {
                DisplayMode = displayMode,
                Notes = task.Result
            });
        }
        /// <summary>
        /// Меняет статус записи и возвращает обратно частичное представление с отображением записей в зависимости от установленного режима
        /// </summary>
        /// <param name="isDone">статус записи</param>
        /// <param name="noteId">ID записи, у которой необходимо сменить статус</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult ChangeNoteStatus(bool isDone, int noteId)
        {
            Settings settings = Session["Settings"] as Settings ?? new Settings { DateBook = new DateBookSettings() };

            DateBookTableViewModel viewModel = new DateBookTableViewModel
            {
                DisplayMode = settings.DateBook.DisplayMode,
                Notes = null
            };
            if (_serviceDb.ChangeNoteStatus(isDone, noteId))
            {
                Task<IEnumerable<Note>> task = Task.Run(async () => await _serviceDb.GetNotesAsync(viewModel.DisplayMode, settings.DateBook.Date));
                viewModel.Notes = task.Result;
            }
            return PartialView("_DateBookTable", viewModel);
        }
        /// <summary>
        /// Удаляет запись и возвращает обратно частичное представление с отображением записей в зависимости от установленного режима
        /// </summary>
        /// <param name="noteId">Id записи, которую необходимо удалить</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult DeleteNote(int noteId)
        {
            Settings settings = Session["Settings"] as Settings ?? new Settings { DateBook = new DateBookSettings() };
            DateBookTableViewModel viewModel = new DateBookTableViewModel
            {
                DisplayMode = settings.DateBook.DisplayMode,
                Notes = null
            };
            if (_serviceDb.DeleteNote(noteId))
            {
                Task<IEnumerable<Note>> task = Task.Run(async () => await _serviceDb.GetNotesAsync(viewModel.DisplayMode, settings.DateBook.Date));
                viewModel.Notes = task.Result;
            }
            return PartialView("_DateBookTable", viewModel);
        }
        /// <summary>
        /// Создает запись, и возвращает частичное представление с результатом
        /// </summary>
        /// <param name="note">ViewModel записи</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult CreateNote(NoteFormData note)
        {
            if (note.NoteType == NoteType.Meeting)
            {
                if (string.IsNullOrEmpty(note.MeetingPlace)) ModelState.AddModelError("MeetingPlace", "Для типа заметки \"Встреча\" необходимо заполнить место встречи");
            }
            if(!DateTime.TryParse(note.StartingDate,out DateTime startDate)) ModelState.AddModelError("StartingDate", "Указана некорректная дата");
            DateTime? endDate = null;
            if (note.NoteType != NoteType.Reminder)
            {
                if (!DateTime.TryParse(note.EndingDate, out DateTime endingDate)) 
                { 
                    ModelState.AddModelError("EndingDate", "Указана некорректная дата");
                    endDate = endingDate;
                }
            }

            if (ModelState.IsValid)
            {
                Note noteToDb = new Note
                {
                    NoteType = (byte)note.NoteType,
                    StartingDate = startDate,
                    SubjectNote = note.SubjectNote,
                    EndingDate = endDate,
                    MeetingPlace = note.MeetingPlace
                };
                Task<bool> task = Task.Run(async () => await _serviceDb.CreateNoteAsync(noteToDb));
                bool result = task.Result;
                TempData["CreatingResult"] = result;
                return PartialView("_AddNoteResult");
            }
            else return PartialView("_CreateNoteForm", note);
        }
        /// <summary>
        /// Сохраняет измененную запись и возвращает частичное представление с результатом
        /// </summary>
        /// <param name="editFormData">ViewModel редактирования записи</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SaveChangesNote(NoteEditFormData editFormData)
        {
            if (editFormData.NoteType == NoteType.Meeting)
            {
                if (string.IsNullOrEmpty(editFormData.MeetingPlace)) ModelState.AddModelError("MeetingPlace", "Для типа заметки \"Встреча\" необходимо заполнить место встречи");
            }
            if (!DateTime.TryParse(editFormData.StartingDate, out DateTime startDate)) ModelState.AddModelError("StartingDate", "Указана некорректная дата");
            DateTime? endDate = null;
            if (editFormData.NoteType != NoteType.Reminder)
            {
                if (!DateTime.TryParse(editFormData.EndingDate, out DateTime endingDate))
                {
                    ModelState.AddModelError("EndingDate", "Указана некорректная дата");
                    endDate = endingDate;
                }
            }

            if (ModelState.IsValid)
            {
                Note noteToDb = new Note
                {
                    Id = editFormData.NoteId,
                    NoteType = (byte)editFormData.NoteType,
                    StartingDate = startDate,
                    SubjectNote = editFormData.SubjectNote,
                    EndingDate = endDate,
                    MeetingPlace = (editFormData.NoteType != NoteType.Meeting)? null : editFormData.MeetingPlace
                };
                Task<bool> task = Task.Run(async () => await _serviceDb.SaveNoteAsync(noteToDb));
                bool result = task.Result;
                TempData["EditingResult"] = result;
                return PartialView("_EditNoteResult");
            }
            else return PartialView("_EditNoteForm", editFormData);
        }
        /// <summary>
        /// Сортирует записи по типу заметки
        /// </summary>
        /// <param name="noteType">Тип заметки</param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        public PartialViewResult SortNoteByType(NoteType noteType)
        {
            Task<IEnumerable<Note>> task = Task.Run(async () => await _serviceDb.GetNotesByTypeAsync((int)noteType));

            //Если нужно с учетом режима то можно использовать код ниже
            //Settings settings = Session["Settings"] as Settings ?? new Settings { DateBook = new DateBookSettings() };
            //Task<IEnumerable<Note>> tsk = Task.Run(async () => await _serviceDb.GetNotesAsync(settings.DateBook.DisplayMode, settings.DateBook.Date));
            //IEnumerable<Note> notesList = tsk.Result;
            //notesList = notesList.AsQueryable().Where(e => e.NoteType == (int)noteType).ToList();


            return PartialView("_SearchResults", task.Result);
        }
        /// <summary>
        /// Возвращает частичное представление с результатами поиска по заметкам
        /// </summary>
        /// <param name="searchText">строка поиска</param>
        /// <param name="searchType">тип поиска</param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        public PartialViewResult SearchNotes(string searchText, NoteSearchType searchType)
        {
            Task<IEnumerable<Note>> task = Task.Run(async () => await _serviceDb.SearchNotesAsync(searchType, searchText));

            return PartialView("_SearchResults", task.Result);
        }

        protected override void Dispose(bool disposing)
        {
            _serviceDb.Dispose();
            base.Dispose(disposing);
        }
    }
}