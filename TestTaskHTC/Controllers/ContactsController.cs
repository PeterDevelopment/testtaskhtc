﻿using DbRepository;
using DbRepository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TestTaskHTC.Models;
using TestTaskHTC.Models.Validation;

namespace TestTaskHTC.Controllers
{
    public class ContactsController : Controller
    {
        private readonly ContactDbService _serviceDb;
        private readonly ContactValidationProvider _validation;
        public ContactsController()
        {
            _serviceDb = new ContactDbService();
            _validation = new ContactValidationProvider();
        }
        /// <summary>
        /// Возвращает список контактов
        /// </summary>
        /// <returns></returns>
        public PartialViewResult GetContacts()
        {
            Task<IEnumerable<Person>> task = Task.Run(async () => await _serviceDb.GetContacts());

            return PartialView("_ContactList", task.Result);
        }
        /// <summary>
        /// Возвращает форму добавления контакта
        /// </summary>
        /// <returns></returns>
        public PartialViewResult ShowAddForm() => PartialView("_AddContact", null);
        /// <summary>
        /// Возвращает форму редактирования контакта по его Id
        /// </summary>
        /// <param name="personId">Id контакта</param>
        /// <returns></returns>
        public PartialViewResult ShowEditForm(int personId)
        {
            Task<Person> task = Task.Run(async () => await _serviceDb.GetPersonAsync(personId));
            return PartialView("_EditContact", task.Result);
        }
        
        /// <summary>
        /// Возвращает информацию по определенному контакту
        /// </summary>
        /// <param name="personId"></param>
        /// <returns></returns>
        public PartialViewResult GetContactDetails(int personId)
        {
            Task<Person> task = Task.Run(async () => await _serviceDb.GetPersonAsync(personId));
            Person person = task.Result;
            person.Contacts = person.Contacts.OrderBy(e => e.ContactType).ToList();
            return PartialView("_PersonDetails", person);
        }

        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SaveContact(Person person)
        {
            if (person.Contacts != null)
            {
                IEnumerable<ValidationErrorMessage> errors = _validation.ValidateContacts(person.Contacts);
                if(errors.Count()!=0)
                {
                    foreach (var item in errors)
                    {
                        ModelState.AddModelError(item.Key, item.Message);
                    }
                }
            }
            else
            {
                person.Contacts = new List<PersonContact>();
                ModelState.AddModelError("Contacts", "Отсутствует контактная информация");
            }

            if (ModelState.IsValid)
            {
                Task<bool> task = Task.Run(async () => await _serviceDb.SaveContactAsync(person));
                if (task.Result) return PartialView("_AddEditContactResult", new ResultOperationMessage { ForAdding = false, IsSuccess = true, Message = "Контакт успешно изменен" });
                else return PartialView("_AddEditContactResult", new ResultOperationMessage { ForAdding = false, Message = "Контакт не изменен" });

            }
            else return PartialView("_EditContact", person);
        }

        /// <summary>
        /// Удаляет Контакт и возвращает список контактов
        /// </summary>
        /// <param name="personId">Id контакта</param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        public PartialViewResult DeletePerson(int personId)
        {
            Task<bool> task = Task.Run(async () => await _serviceDb.DeleteContactAsync(personId));
            if (!task.Result)
            {
                ViewBag.DeletingError = "Не удалось удалить запись";
            }
            Task<IEnumerable<Person>> getPersons = Task.Run(async () => await _serviceDb.GetContacts());
            return PartialView("_ContactList", getPersons.Result);
        }

        /// <summary>
        /// Добавляет контакт
        /// </summary>
        /// <param name="person">объект Person</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult AddContact(Person person)
        {
            if (person.Contacts != null)
            {
                IEnumerable<ValidationErrorMessage> errors = _validation.ValidateContacts(person.Contacts);
                if (errors.Count() != 0)
                {
                    foreach (var item in errors)
                    {
                        ModelState.AddModelError(item.Key, item.Message);
                    }
                }
            }
            else
            {
                person.Contacts = new List<PersonContact>();
                ModelState.AddModelError("Contacts", "Отсутствует контактная информация");
            } 

            if (ModelState.IsValid)
            {
                Task<bool> task = Task.Run(async () => await _serviceDb.AddContactAsync(person));
                if (task.Result) return PartialView("_AddEditContactResult", new ResultOperationMessage { ForAdding = true, IsSuccess = true, Message= "Контакт успешно создан" });
                else return PartialView("_AddEditContactResult", new ResultOperationMessage {ForAdding=true, Message = "Контакт не добавлен" });

            }
            else return PartialView("_AddContact", person);
        }

        /// <summary>
        /// Осуществляет поиск среди контактов по строке поиска
        /// </summary>
        /// <param name="searchText">строика поиска</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SearchContact(string searchText)
        {
            IEnumerable<Person> people = new List<Person>();
            if (!string.IsNullOrEmpty(searchText))
            {
                Task<IEnumerable<Person>> searchResult = Task.Run(async () => await _serviceDb.SearchPeopleAsync(searchText));
                people = searchResult.Result;
            }
            else
            {
                Task<IEnumerable<Person>> allPeople = Task.Run(async () => await _serviceDb.GetContacts());
                people = allPeople.Result;
            }
            return PartialView("_ContactList", people);
        }

        protected override void Dispose(bool disposing)
        {
            _serviceDb.Dispose();
            base.Dispose(disposing);
        }
    }
}