﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestTaskHTC.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Возвращает главную страницу сайта
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.Title = "Главная";
            return View();
        }
        /// <summary>
        /// Возвращает страницу раздела ежедневника
        /// </summary>
        /// <returns></returns>
        public ActionResult DateBook()
        {
            ViewBag.Title = "DateBook";
            return View();
        }
        /// <summary>
        /// Возвращает страницу раздела Списка контактов
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            ViewBag.Title = "Список контактов";
            return View();
        }
    }
}